import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import Layout from "./containers/Layout/Layout";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AllProducts from "./containers/AllProducts/AllProducts";
import ProductItem from "./containers/ProductItem/ProductItem";
import AddNewProduct from "./containers/AddNewProduct/AddNewProduct";


class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
          <Route path="/" exact component={AllProducts} />
          <Route path="/categories/:id" exact component={AllProducts} />
          <Route path="/items/:id" exact component={ProductItem} />
          <Route path="/add-new-product" exact component={AddNewProduct} />
          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route render={() => <h1 style={{textAlign: 'center'}}>Page not found</h1>} />
        </Switch>
      </Layout>
    );
  }
}

export default App;
