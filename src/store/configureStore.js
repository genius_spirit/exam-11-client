import { routerMiddleware, routerReducer } from "react-router-redux";
import thunkMiddleware from "redux-thunk";
import createHistory from "history/createBrowserHistory";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import usersReducer from "./reducers/users";
import productsReducer from './reducers/products';
import categoriesReducer from './reducers/categories';
import { readState, saveState } from "./localStorage";

const rootReducer = combineReducers({
  users: usersReducer,
  products: productsReducer,
  categories: categoriesReducer,
  routing: routerReducer
});

export const history = createHistory();

const middleware = [
  thunkMiddleware,
  routerMiddleware(history)
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(applyMiddleware(...middleware));

const persistedState = readState();

const store = createStore(rootReducer, persistedState, enhancer);

store.subscribe(() => {
  saveState({users: {user: store.getState().users.user}});
});

export default store;