import { GET_CATEGORIES_FAILURE, GET_CATEGORIES_SUCCESS } from "../actions/actionTypes";

const initialState = {
  categories: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_CATEGORIES_SUCCESS:
      return {...state, categories: action.categories};
    case GET_CATEGORIES_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;