import {
  ADD_NEW_PRODUCT_FAILURE,
  GET_ITEM_FAILURE,
  GET_ITEM_SUCCESS,
  GET_PRODUCTS_BY_CATEGORY_FAILURE,
  GET_PRODUCTS_BY_CATEGORY_SUCCESS,
  GET_PRODUCTS_FAILURE,
  GET_PRODUCTS_SUCCESS
} from "../actions/actionTypes";

const initialState = {
  products: [],
  error: null,
  item: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PRODUCTS_SUCCESS:
      return {...state, products: action.products};
    case GET_PRODUCTS_FAILURE:
      return {...state, error: action.error};
    case GET_PRODUCTS_BY_CATEGORY_SUCCESS:
      return {...state, products: action.products};
    case GET_PRODUCTS_BY_CATEGORY_FAILURE:
      return {...state, error: action.error};
    case GET_ITEM_SUCCESS:
      return {...state, item: action.item};
    case GET_ITEM_FAILURE:
      return {...state, error: action.error};
    case ADD_NEW_PRODUCT_FAILURE:
      return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;