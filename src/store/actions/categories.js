import axios from "../../axios-api";

import { GET_CATEGORIES_FAILURE, GET_CATEGORIES_SUCCESS } from "./actionTypes";

const getCategoriesSuccess = categories => {
  return {type: GET_CATEGORIES_SUCCESS, categories};
};

const getCategoriesFailure = error => {
  return {type: GET_CATEGORIES_FAILURE, error};
};

export const getCategories = () => {
  return dispatch => {
    return axios.get('/categories').then(
      response => {
        dispatch(getCategoriesSuccess(response.data));
      },
      error => {
        dispatch(getCategoriesFailure(error));
      }
    );
  };
};

