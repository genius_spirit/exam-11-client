import axios from "../../axios-api";

import { push } from "react-router-redux";

import {
  ADD_NEW_PRODUCT_FAILURE,
  ADD_NEW_PRODUCT_SUCCESS,
  GET_ITEM_FAILURE,
  GET_ITEM_SUCCESS,
  GET_PRODUCTS_BY_CATEGORY_FAILURE,
  GET_PRODUCTS_BY_CATEGORY_SUCCESS,
  GET_PRODUCTS_SUCCESS
} from "./actionTypes";
import { GET_PRODUCTS_FAILURE } from "./actionTypes";

const getProductsSuccess = (products) => {
  return {type: GET_PRODUCTS_SUCCESS, products};
};

const getProductsFailure = error => {
  return {type: GET_PRODUCTS_FAILURE, error};
};

export const getAllProducts = () => {
  return dispatch => {
    return axios.get('/products').then(
      response => {
        dispatch(getProductsSuccess(response.data));
      },
      error => {
        dispatch(getProductsFailure(error));
      }
    );
  };
};

const getProductsByCategorySuccess = (products) => {
  return {type: GET_PRODUCTS_BY_CATEGORY_SUCCESS, products};
};

const getProductsByCategoryFailure = error => {
  return {type: GET_PRODUCTS_BY_CATEGORY_FAILURE, error};
};

export const getProductsByCategory = id => {
  return dispatch => {
    return axios.get(`/products/categories/${id}`).then(
      response => {
        dispatch(getProductsByCategorySuccess(response.data));
      },
      error => {
        dispatch(getProductsByCategoryFailure(error));
      }
    );
  };
};

const getItemSuccess = item => {
  return {type: GET_ITEM_SUCCESS, item};
};

const getItemFailure = error => {
  return {type: GET_ITEM_FAILURE, error};
};

export const getItem = id => {
  return (dispatch) => {
    return axios.get(`/products/${id}`).then(
      response => {
        dispatch(getItemSuccess(response.data));
      },
      error => {
        dispatch(getItemFailure(error));
      }
    );
  };
};

const addNewProductSuccess = () => {
  return {type: ADD_NEW_PRODUCT_SUCCESS};
};

const addNewProductFailure = (error) => {
  return {type: ADD_NEW_PRODUCT_FAILURE, error};
};

export const addNewProduct = (postData) => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    return axios.post('/products', postData, {headers}).then(
      response => {
        dispatch(addNewProductSuccess());
        dispatch(push('/'));
      },
      error => {
        dispatch(addNewProductFailure(error));
      }
    )
  }
};

