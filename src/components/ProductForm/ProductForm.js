import React, { Component } from "react";
import { Button, Col, ControlLabel, Form, FormControl, FormGroup } from "react-bootstrap";
import { connect } from "react-redux";

class ProductForm extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    category: '',
    price: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

   this.props.onSubmit(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };

  render() {
    return (
      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="newTitle">
          <Col componentClass={ControlLabel} sm={2}>Title</Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter title"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="newPrice">
          <Col componentClass={ControlLabel} sm={2}>Price</Col>
          <Col sm={10}>
            <FormControl
              type="number" required
              placeholder="Enter price"
              name="price"
              value={this.state.price}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="newDescription">
          <Col componentClass={ControlLabel} sm={2}>Description</Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder="Enter description"
              name="description"
              value={this.state.description}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="newImage">
          <Col componentClass={ControlLabel} sm={2}>Image</Col>
          <Col sm={10}>
            <FormControl
              type="file"
              name="image"
              onChange={this.fileChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="newCategory">
          <Col componentClass={ControlLabel} sm={2}>Image</Col>
          <Col sm={10}>
            <FormControl componentClass="select"
                         name="category"
                         placeholder="select"
                         value={this.state.category}
                         onChange={this.inputChangeHandler}>
              <option value="select">select</option>
              {this.props.categories.map(item => (
                <option key={item._id} value={item._id}>{item.title}</option>
              ))}
            </FormControl>
          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Save</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    categories: state.categories.categories
  }
};

export default connect(mapStateToProps)(ProductForm);