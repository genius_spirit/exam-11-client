import React from "react";
import { Menu } from "semantic-ui-react";
import {Link} from "react-router-dom";

const UserMenu = ({ user, logout }) => {
  return (
    <Menu.Menu position="right" >
      <Menu.Item>Hello, {user.username}</Menu.Item>
      <Menu.Item>
        <Link to="/add-new-product">
          Add new product
        </Link>
      </Menu.Item>
      <Menu.Item onClick={logout} >
         Logout
      </Menu.Item>
    </Menu.Menu>
  );
};

export default UserMenu;
