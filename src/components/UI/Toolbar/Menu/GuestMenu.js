import React from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

const GuestMenu = () => {
  return (
    <Menu.Menu position="right">
      <Menu.Item>
        <Link to="/register">
          Register
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/login">
          Login
        </Link>
      </Menu.Item>
    </Menu.Menu>
  );
};

export default GuestMenu;
