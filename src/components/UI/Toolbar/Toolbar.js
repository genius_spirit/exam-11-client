import React from "react";
import { Menu } from "semantic-ui-react";
import { Link } from "react-router-dom";

import UserMenu from "./Menu/UserMenu";
import GuestMenu from "./Menu/GuestMenu";

const Toolbar = ({user, logout, categories}) => {
  return (
    <Menu style={{ marginBottom: '20px' }}>
      <Menu.Item>
        <Link to="/">
          Tech Market
        </Link>
      </Menu.Item>
      {categories.map(item => (
        <Menu.Item key={item._id}>
          <Link to={`/categories/${item._id}`}>
            {item.title}
          </Link>
        </Menu.Item>
      ))}
      {user ? <UserMenu user={user} logout={logout}/> : <GuestMenu/>}
    </Menu>
  );
};

export default Toolbar;
