import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import {
  Button,
  Col,
  Form,
  FormGroup,
  PageHeader
} from "react-bootstrap";
import { registerUser } from "../../store/actions/users";
import FormElement from "../../components/UI/Form/FormElement/FormElement";

class Register extends Component {
  state = {
    username: "",
    password: "",
    name: "",
    phone: ""
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  submitFormHandler = event => {
    event.preventDefault();

    this.props.onRegisterUser(this.state);
  };

  hasErrorForField = fieldName => {
    return this.props.error && this.props.error.errors[fieldName];
  };

  render() {
    return (
      <Fragment>
        <PageHeader>Register new user</PageHeader>
        <Form horizontal onSubmit={this.submitFormHandler}>

          <FormElement
            required={true}
            propertyName="username"
            title="Username"
            type="text"
            value={this.state.username}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-username"
            error={this.hasErrorForField('username') && this.props.error.errors.username.message}
          />

          <FormElement
            required={true}
            propertyName="password"
            title="Password"
            type="password"
            value={this.state.password}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-password"
            error={this.hasErrorForField('password') && this.props.error.errors.password.message}
          />
          
          <FormElement
            propertyName="name"
            title="Display name"
            type="text"
            value={this.state.name}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-name"
            required={true}
            />

          <FormElement
            propertyName="phone"
            title="Phone number"
            type="text"
            value={this.state.phone}
            changeHandler={this.inputChangeHandler}
            autoComplete="new-phone"
            required={true}
            />

          <FormGroup>
            <Col smOffset={2} sm={10}>
              <Button bsStyle="primary" type="submit">
                Register
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.users.registerError
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onRegisterUser: userData => dispatch(registerUser(userData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);
