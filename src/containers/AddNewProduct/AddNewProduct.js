import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { PageHeader } from "react-bootstrap";
import ProductForm from "../../components/ProductForm/ProductForm";
import { addNewProduct } from "../../store/actions/products";

class AddNewProduct extends Component {

  render() {
    return (
      <Fragment>
        <PageHeader>Add new product</PageHeader>
        <ProductForm onSubmit={this.props.addNewProduct}/>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addNewProduct: postData => dispatch(addNewProduct(postData))
  };
};


export default connect(null, mapDispatchToProps)(AddNewProduct);
