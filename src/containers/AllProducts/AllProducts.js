import React, {Component} from 'react';
import {connect} from 'react-redux';

import { getAllProducts, getProductsByCategory } from "../../store/actions/products";
import config from "../../config";
import { Card, Image } from "semantic-ui-react";

class AllProducts extends Component {

  componentDidMount() {
    if (this.props.location.search === '') {
      this.props.getAllProducts();
    }
  };

  componentWillReceiveProps
  (nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      if (nextProps.match.params.id) {
      this.props.getProductsByCategory(nextProps.match.params.id);
      } else {
        this.props.getAllProducts();
      }
    }
  }

  render() {
    return (
      <React.Fragment>
        <Card.Group centered stackable>
        {this.props.products && this.props.products.map(item =>(
          <Card key={item._id} onClick={() => this.props.history.push('/items/' + item._id)}>
            <Card.Content>
              <Image src={config.apiUrl + '/uploads/' + item.image}
                     size="tiny"
                     style={{marginBottom: '15px'}}
                     centered/>
              <Card.Header>{item.title}</Card.Header>
              <Card.Description>{item.price + ' USD'}</Card.Description>
            </Card.Content>
          </Card>
        ))}
        </Card.Group>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.products.products,
    user: state.users.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getAllProducts: () => dispatch(getAllProducts()),
    getProductsByCategory: id => dispatch(getProductsByCategory(id)),
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(AllProducts);
