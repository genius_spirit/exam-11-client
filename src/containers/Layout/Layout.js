import React, {Component} from "react";
import { connect } from "react-redux";
import { NotificationContainer } from "react-notifications";
import { Container } from "semantic-ui-react";

import Toolbar from "../../components/UI/Toolbar/Toolbar";
import { logoutUser } from "../../store/actions/users";

import 'react-notifications/lib/notifications.css';
import { getCategories } from "../../store/actions/categories";

class Layout extends Component {

  componentDidMount() {
    this.props.getCategories()
  }

  render() {
    return (
      <Container>
        <NotificationContainer/>
        <header>
          <Toolbar user={this.props.user}
                   logout={this.props.logoutUser}
                   categories={this.props.categories}
          />
        </header>
        <main>
          {this.props.children}
        </main>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.users.user,
    categories: state.categories.categories
  }
};

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(logoutUser()),
  getCategories: () => dispatch(getCategories())
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);

