import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { getItem } from "../../store/actions/products";
import { Image, Panel } from "react-bootstrap";
import config from "../../config";

class ProductItem extends Component {

  componentDidMount() {
    this.props.getItem(this.props.match.params.id)
  }

  render() {
    return (
      <Fragment>
        {this.props.item ?
          <Panel>
            <Panel.Heading style={{fontSize: '20px'}}>
              {this.props.item.title}
            </Panel.Heading>
                <Panel.Body>
                  {this.props.item.image &&
                  <Image style={{width: '150px', textAlign: 'center'}}
                         src={config.apiUrl + '/uploads/' + this.props.item.image}/>}
                </Panel.Body>
            <Panel.Body><strong>Price: {this.props.item.price} USD</strong></Panel.Body>
                <Panel.Body> {this.props.item.description}</Panel.Body>
          </Panel> : null
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    item: state.products.item
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getItem: id => dispatch(getItem(id))
  };
};


export default connect(mapStateToProps, mapDispatchToProps)(ProductItem);
